package com.example.nycschooldemo.di.schooldetailmodule

import com.example.nycschooldemo.data.schooldetail.SchoolSATDetailRepository
import com.example.nycschooldemo.data.schooldetail.SchoolSATDetailDataSource
import com.example.nycschooldemo.data.schooldetail.SchoolSATDetailRemoteDataSource
import com.example.nycschooldemo.network.SchoolSATDetailAPIService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object SchoolDetailDataSourceModule {

    @Singleton
    @Provides
    fun provideSchoolDataSource(apiService: SchoolSATDetailAPIService): SchoolSATDetailDataSource {
        return SchoolSATDetailRemoteDataSource(apiService)
    }

    @Singleton
    @Provides
    fun provideSchoolSATDetailRepository(dataSource: SchoolSATDetailDataSource): SchoolSATDetailRepository {
        return SchoolSATDetailRepository(dataSource)
    }

}