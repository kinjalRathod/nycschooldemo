package com.example.nycschooldemo.data.schoollist

import com.example.nycschooldemo.data.model.School
import com.example.nycschooldemo.network.SchoolAPIService
import com.example.nycschooldemo.network.toUIState
import com.example.nycschooldemo.ui.UIState
import javax.inject.Inject

class SchoolRemoteDataSource
@Inject constructor(private val api: SchoolAPIService) : SchoolDataSource {

    override suspend fun getSchools(): UIState<List<School>> {
        return try {
            api.getNYCSchools().toUIState()
        } catch (e: Exception) {
            e.printStackTrace()
            UIState.Error(e)
        }
    }

}