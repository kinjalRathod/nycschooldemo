package com.example.nycschooldemo

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.nycschooldemo.data.schoollist.SchoolRepository
import com.example.nycschooldemo.data.model.School
import com.example.nycschooldemo.ui.UIState
import com.example.nycschooldemo.viewmodels.SchoolViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.setMain
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`

internal class SchoolViewModelTest {

    private lateinit var schoolViewModel: SchoolViewModel
    private lateinit var schoolRepository: SchoolRepository

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @OptIn(ExperimentalCoroutinesApi::class)
    @Before
    fun setup() {
        Dispatchers.setMain(Dispatchers.Unconfined)

        schoolRepository = mock(SchoolRepository::class.java)
        schoolViewModel = SchoolViewModel(schoolRepository)
    }

    @Test
    fun getSchoolListSuccess() = runBlocking {
        // Arrange
        val schools = listOf(
            School(schoolName = "School A"),
            School(schoolName = "School B")
        )
        `when`(schoolRepository.getSchools()).thenReturn(
            flowOf(UIState.Success(schools))
        )

        // Act
        // Call the function under test
        schoolViewModel.getSchools()

        // Assert
        val uiState = schoolViewModel.list.value
        assert(uiState is UIState.Success)
        assertEquals(schools.size, (uiState as UIState.Success).data.size)
    }

}