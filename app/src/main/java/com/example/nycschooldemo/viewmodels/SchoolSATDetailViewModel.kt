package com.example.nycschooldemo.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nycschooldemo.data.schooldetail.SchoolSATDetailRepository
import com.example.nycschooldemo.data.model.School
import com.example.nycschooldemo.data.model.SchoolSATDetail
import com.example.nycschooldemo.ui.UIState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SchoolSATDetailViewModel @Inject constructor(
    private val repository: SchoolSATDetailRepository,
) : ViewModel() {

    private val _satResults = MutableStateFlow<UIState<List<SchoolSATDetail>>>(UIState.Loading)

    val satResults: StateFlow<UIState<List<SchoolSATDetail>>>
        get() = _satResults

    private val selectedSchoolDetail = MutableStateFlow(School())

    private fun getSatResult() {
        viewModelScope.launch {
            repository.getSATResults(selectedSchoolDetail.value.dbn ?: "").collect {
                _satResults.value = it
            }
        }
    }

    fun setData(selectedSchoolDetail: School) {
        this.selectedSchoolDetail.value = selectedSchoolDetail
        getSatResult()
    }

}
