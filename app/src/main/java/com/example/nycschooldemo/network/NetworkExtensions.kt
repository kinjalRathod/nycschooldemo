package com.example.nycschooldemo.network

import com.example.nycschooldemo.exception.APIException
import com.example.nycschooldemo.ui.UIState
import retrofit2.Response


/**
 * Transforms a Retrofit Response object to a data holder UIState object
 * */
fun <T> Response<T>.toUIState(): UIState<T> {
    return try {
        if (isSuccessful) {
            val body = body()
            if (body != null) {
                UIState.Success(body)
            } else UIState.Error(Throwable("Empty body"))
        } else {
            UIState.Error(APIException(errorBody()?.string(), code()))
        }
    } catch (e: Exception) {
        UIState.Error(e)
    }
}