package com.example.nycschooldemo.data.schooldetail

import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class SchoolSATDetailRepository @Inject constructor(private val schoolDataSource: SchoolSATDetailDataSource) {

    suspend fun getSATResults(dbn: String) = flow {
        emit(schoolDataSource.getSchoolSATDetail(dbn))
    }

}