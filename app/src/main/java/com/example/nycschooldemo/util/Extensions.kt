package com.example.nycschooldemo.util

import android.content.Context
import android.content.Intent
import android.net.Uri
import java.util.*


fun Context.openGoogleMaps(latitude: Float, longitude: Float) {
    val uri: String = java.lang.String.format(Locale.ENGLISH, "geo:%f,%f", latitude, longitude)
    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
    startActivity(intent)
}

fun Context.openDial(phoneNumber: String) {
    val intent = Intent(Intent.ACTION_DIAL)
    intent.data = Uri.parse("tel:$phoneNumber")
    startActivity(intent)
}

fun Context.openEmail(email: String) {
    val intent = Intent(Intent.ACTION_VIEW, Uri.parse("mailto: $email"))
    startActivity(intent)
}