package com.example.nycschooldemo.data.schoollist

import com.example.nycschooldemo.data.model.School
import com.example.nycschooldemo.data.model.SchoolSATDetail
import com.example.nycschooldemo.ui.UIState
import retrofit2.Response

interface SchoolDataSource {

    suspend fun getSchools(): UIState<List<School>>

}