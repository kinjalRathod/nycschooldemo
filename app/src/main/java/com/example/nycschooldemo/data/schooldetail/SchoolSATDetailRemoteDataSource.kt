package com.example.nycschooldemo.data.schooldetail

import com.example.nycschooldemo.data.model.SchoolSATDetail
import com.example.nycschooldemo.network.SchoolSATDetailAPIService
import com.example.nycschooldemo.network.toUIState
import com.example.nycschooldemo.ui.UIState
import javax.inject.Inject

class SchoolSATDetailRemoteDataSource
@Inject constructor(private val api: SchoolSATDetailAPIService) : SchoolSATDetailDataSource {

    override suspend fun getSchoolSATDetail(dbn: String): UIState<List<SchoolSATDetail>> {
        return try {
            api.getNYCSchoolSATResults(dbn).toUIState()
        } catch (e: Exception) {
            UIState.Error(e)
        }
    }

}