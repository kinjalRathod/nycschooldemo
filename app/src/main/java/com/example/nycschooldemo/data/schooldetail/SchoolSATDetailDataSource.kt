package com.example.nycschooldemo.data.schooldetail

import com.example.nycschooldemo.data.model.SchoolSATDetail
import com.example.nycschooldemo.ui.UIState
import retrofit2.Response

interface SchoolSATDetailDataSource {

    suspend fun getSchoolSATDetail(dbn: String): UIState<List<SchoolSATDetail>>

}