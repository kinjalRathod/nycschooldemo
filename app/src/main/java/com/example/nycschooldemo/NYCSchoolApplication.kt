package com.example.nycschooldemo

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class NYCSchoolApplication : Application() {

    override fun onCreate() {
        super.onCreate()
    }

}