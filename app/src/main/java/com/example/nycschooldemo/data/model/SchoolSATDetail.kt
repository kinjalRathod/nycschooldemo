package com.example.nycschooldemo.data.model

import com.squareup.moshi.Json

data class SchoolSATDetail(
    @Json(name = "dbn") val dbn: String? = null,
    @Json(name = "school_name") val schoolName: String? = null,
    @Json(name = "num_of_sat_test_takers") val satTestTakers: String? = null,
    @Json(name = "sat_critical_reading_avg_score") val criticalReadingAvgScore: String? = null,
    @Json(name = "sat_math_avg_score") val mathAvgScore: String? = null,
    @Json(name = "sat_writing_avg_score") val writingAvgScore: String? = null,
)