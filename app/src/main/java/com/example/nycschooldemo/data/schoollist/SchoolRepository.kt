package com.example.nycschooldemo.data.schoollist

import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class SchoolRepository @Inject constructor(private val schoolDataSource: SchoolDataSource) {

    suspend fun getSchools() =
        flow {
            emit(schoolDataSource.getSchools())
        }

}