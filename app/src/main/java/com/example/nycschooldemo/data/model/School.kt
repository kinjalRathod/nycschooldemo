package com.example.nycschooldemo.data.model

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.parcelize.Parcelize

@Parcelize
data class School(
    @Json(name = "dbn") val dbn: String? = null,
    @Json(name = "boro") val boro: String? = null,
    @Json(name = "neighborhood") val neighborhood: String? = null,
    @Json(name = "location") val location: String? = null,
    @Json(name = "school_name") val schoolName: String? = null,
    @Json(name = "overview_paragraph") val overviewParagraph: String? = null,
    @Json(name = "school_10th_seats") val school10ThSeats: String? = null,
    @Json(name = "academicopportunities1") val academicOpportunities1: String? = null,
    @Json(name = "academicopportunities2") val academicOpportunities2: String? = null,
    @Json(name = "ell_programs") val ellPrograms: String? = null,
    @Json(name = "building_code") val buildingCode: String? = null,
    @Json(name = "phone_number") val phoneNumber: String? = null,
    @Json(name = "fax_number") val faxNumber: String? = null,
    @Json(name = "school_email") val schoolEmail: String? = null,
    @Json(name = "website") val website: String? = null,
    @Json(name = "subway") val subway: String? = null,
    @Json(name = "bus") val bus: String? = null,
    @Json(name = "grades2018") val grades2018: String? = null,
    @Json(name = "finalgrades") val finalgrades: String? = null,
    @Json(name = "total_students") val totalStudents: String? = null,
    @Json(name = "extracurricular_activities") val extracurricularActivities: String? = null,
    @Json(name = "school_sports") val schoolSports: String? = null,
    @Json(name = "offerRate1") val offerRate1: String? = null,
    @Json(name = "program1") val program1: String? = null,
    @Json(name = "code1") val code1: String? = null,
    @Json(name = "interest1") val interest1: String? = null,
    @Json(name = "method1") val method1: String? = null,
    @Json(name = "seats9Ge1") val seats9Ge1: String? = null,
    @Json(name = "grade9Gefilledflag1") val grade9Gefilledflag1: String? = null,
    @Json(name = "grade9Geapplicants1") val grade9Geapplicants1: String? = null,
    @Json(name = "seats9Swd1") val seats9Swd1: String? = null,
    @Json(name = "grade9Swdfilledflag1") val grade9Swdfilledflag1: String? = null,
    @Json(name = "grade9Swdapplicants1") val grade9Swdapplicants1: String? = null,
    @Json(name = "primary_address_line_1") val primaryAddressLine1: String? = null,
    @Json(name = "city") val city: String? = null,
    @Json(name = "zip") val zip: String? = null,
    @Json(name = "state_code") val stateCode: String? = null,
    @Json(name = "latitude") val latitude: String? = null,
    @Json(name = "longitude") val longitude: String? = null,
    @Json(name = "communityBoard") val communityBoard: String? = null,
    @Json(name = "councilDistrict") val councilDistrict: String? = null,
    @Json(name = "censusTract") val censusTract: String? = null,
    @Json(name = "bin") val bin: String? = null,
    @Json(name = "bbl") val bbl: String? = null,
    @Json(name = "nta") val nta: String? = null,
    @Json(name = "borough") val borough: String? = null
) : Parcelable

fun School.getShortAddress() = "$city, $zip"
fun School.getFullAddress() = "$primaryAddressLine1, $city, $zip"
