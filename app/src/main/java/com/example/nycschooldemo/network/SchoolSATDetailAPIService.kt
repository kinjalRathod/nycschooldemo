package com.example.nycschooldemo.network

import com.example.nycschooldemo.data.model.School
import com.example.nycschooldemo.data.model.SchoolSATDetail
import com.example.nycschooldemo.util.API_PATH
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * API Service to fetch data
 */
interface SchoolSATDetailAPIService {

    /**
     * Fetching SAT results of NYC schools
     */
    @GET("$API_PATH/f9bf-2cp4.json")
    suspend fun getNYCSchoolSATResults(@Query("dbn") dbn: String): Response<List<SchoolSATDetail>>

}