package com.example.nycschooldemo.exception

/**
 * A [Throwable] implementation that holds both error and code
 * @property errorBody
 * @property code
 * */
data class APIException(val errorBody: String?, val code: Int) : Throwable(errorBody)


