package com.example.nycschooldemo.network

import com.example.nycschooldemo.data.model.School
import com.example.nycschooldemo.util.API_PATH
import retrofit2.Response
import retrofit2.http.GET

/**
 * API Service to fetch data
 */
interface SchoolAPIService {

    /**
     * Fetching NYC schools
     */
    @GET("$API_PATH/s3k6-pzi2.json")
    suspend fun getNYCSchools(): Response<List<School>>
}