package com.example.nycschooldemo.ui.theme

import androidx.compose.material.Typography
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.nycschooldemo.R

val notoSans = FontFamily(Font(R.font.notosans_medium))

// Set of Material typography styles to start with
val Typography = Typography(
    body1 = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.Normal,
        fontSize = 16.sp
    )
    /* Other default text styles to override
    button = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.W500,
        fontSize = 14.sp
    ),
    caption = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.Normal,
        fontSize = 12.sp
    )
    */
)


val heading = TextStyle(
    fontFamily = notoSans,
    fontWeight = FontWeight.SemiBold,
    fontSize = 20.sp,
    color = Navy
)

val title = TextStyle(
    fontFamily = notoSans,
    fontWeight = FontWeight.SemiBold,
    fontSize = 18.sp,
    color = PrimaryFontColor
)

val itemTitle = TextStyle(
    fontFamily = notoSans,
    fontWeight = FontWeight.SemiBold,
    fontSize = 16.sp,
    color = PrimaryFontColor
)


/** extraLargeMedium style */
val extraLargeMedium = TextStyle(
    fontFamily = notoSans,
    fontWeight = FontWeight.SemiBold,
    fontSize = 20.sp,
    letterSpacing = 0.sp
)

/** body style */
val body = TextStyle(
    fontFamily = notoSans,
    fontWeight = FontWeight.Normal,
    fontSize = 14.sp,
    letterSpacing = 0.sp,
    color = SecondaryFontColor
)

/** body style */
val scoreText = TextStyle(
    fontFamily = notoSans,
    fontWeight = FontWeight.Bold,
    fontSize = 20.sp
)

/** body style */
val scoreLabel = TextStyle(
    fontFamily = notoSans,
    fontWeight = FontWeight.Normal,
    fontSize = 14.sp,
    color = PrimaryFontColor
)
