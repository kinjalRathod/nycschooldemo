package com.example.nycschooldemo.ui.navigation

sealed class Screen(val route: String) {

    object SchoolList : Screen("SchoolList")

    object SchoolDetail : Screen("SchoolDetail")
}