package com.example.nycschooldemo.ui.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.nycschooldemo.data.model.School
import com.example.nycschooldemo.ui.schooldetailscreen.SchoolDetail
import com.example.nycschooldemo.util.KEY_SCHOOL

@Composable
fun MainNavHost() {
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = Screen.SchoolList.route) {
        // School list screen
        composable(route = Screen.SchoolList.route) {
            com.example.nycschooldemo.ui.schoollistscreen.SchoolList { school ->
                navController.currentBackStackEntry?.savedStateHandle?.set(KEY_SCHOOL, school)
                navController.navigate(Screen.SchoolDetail.route)
            }
        }

        // School Detail Screen
        composable(route = Screen.SchoolDetail.route) {
            val selectedSchool =
                navController.previousBackStackEntry?.savedStateHandle?.get<School>(KEY_SCHOOL)
            SchoolDetail(selectedSchool) {
                navController.popBackStack()
            }
        }
    }
}