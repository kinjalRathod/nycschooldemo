package com.example.nycschooldemo.ui.generic

import androidx.annotation.DrawableRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.example.nycschooldemo.ui.theme.*

@Composable
fun EmptyComponent(
    @DrawableRes iconRes: Int? = null,
    iconSize :Dp = 60.dp,
    header: Int? = null,
    description: Int? = null,
    btnLabel: Int? = null,
    onBtnClick: (() -> Unit)? = null
) {
    Box(
        modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            /** Show Empty Icon or hides for null value */
            if (iconRes != null) {
                Image(painter = painterResource(id = iconRes),
                    contentDescription = null,
                    modifier = Modifier.run {
                        size(iconSize).align(Alignment.CenterHorizontally)
                    })
            }
            /** Show Header only if the header string is passed */
            if (header != null) {
                Text(
                    text = stringResource(id = header),
                    modifier = Modifier
                        .fillMaxWidth()
                        .wrapContentWidth()
                        .padding(top = 18.dp),
                    textAlign = TextAlign.Center,
                    color = PrimaryFontColor1,
                    style = extraLargeMedium
                )
            }
            /** Show description only if the description string is passed */
            if (description != null) {
                Text(
                    text = stringResource(id = description),
                    modifier = Modifier
                        .fillMaxWidth()
                        .wrapContentHeight()
                        .padding(16.dp),
                    textAlign = TextAlign.Center,
                    color = SemiDarkGray,
                    style = body
                )
            }
            /** Show a button only if the button label is passed */
            if (btnLabel != null) {
                Box(
                    modifier = Modifier
                        .padding(18.dp)
                        .wrapContentSize()
                        .clip(RoundedCornerShape(8.dp))
                        .background(Blue)
                ) {
                    Text(modifier = Modifier
                        .clickable { onBtnClick?.invoke() }
                        .padding(
                            horizontal = 24.dp, vertical = 10.dp
                        ),
                        text = stringResource(id = btnLabel),
                        color = White,
                        textAlign = TextAlign.Center)
                }
            }
        }
    }
}
