package com.example.nycschooldemo.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFF163c40)
val Purple500 = Color(0xFF163c40)
val Purple700 = Color(0xFF163c40)
val Teal200 = Color(0xFF03DAC5)
val LighterGray = Color(0xFFF5F5F5)
val Black = Color(0xFF000000)
val Navy = Color(0xFF000040)

/** SemiDarkGray color */
val LightGray = Color(0xFFB4B9C0)
val SemiDarkGray = Color(0xFF62656B)
val White = Color(0xFFFFFFFF)
val PrimaryFontColor = Color(0xFF1A212E)
val PrimaryFontColor1 = Color(0xFF374052)
val SecondaryFontColor = Color(0xFF797d7a)
val bg = Color(0xFFf2f5f7)
val Blue = Color(0xFF51A0A9)
val HyperLink = Color(0xFF5e8cf7)

/**
 * Function to update alpha of the color.
 *
 * @param alpha value (0 to 1) of the alpha that needs to be applied.
 * */
fun Color.withAlpha(alpha: Float): Color {
    val newAlpha: Float = this.alpha * alpha
    return this.copy(newAlpha, red, green, blue)
}