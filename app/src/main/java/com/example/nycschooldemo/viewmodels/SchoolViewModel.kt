package com.example.nycschooldemo.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nycschooldemo.data.schoollist.SchoolRepository
import com.example.nycschooldemo.data.model.School
import com.example.nycschooldemo.ui.UIState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SchoolViewModel @Inject constructor(
    private val repository: SchoolRepository
) : ViewModel() {

    private val _list = MutableStateFlow<UIState<List<School>>>(UIState.Loading)

    val list: StateFlow<UIState<List<School>>>
        get() = _list

    init {
        getSchools()
    }

    fun getSchools() {
        viewModelScope.launch {
            repository.getSchools().collect {
                _list.value = it
            }
        }
    }

    fun refresh() {
        _list.value = UIState.Loading
        getSchools()
    }

}