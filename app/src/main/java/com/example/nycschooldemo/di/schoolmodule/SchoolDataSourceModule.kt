package com.example.nycschooldemo.di.schoolmodule

import com.example.nycschooldemo.data.schoollist.SchoolRepository
import com.example.nycschooldemo.network.SchoolAPIService
import com.example.nycschooldemo.data.schoollist.SchoolDataSource
import com.example.nycschooldemo.data.schoollist.SchoolRemoteDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@InstallIn(SingletonComponent::class)
@Module
object SchoolDataSourceModule {

    @Singleton
    @Provides
    fun provideSchoolDataSource(schoolApiService: SchoolAPIService): SchoolDataSource {
        return SchoolRemoteDataSource(schoolApiService)
    }

    @Singleton
    @Provides
    fun provideSchoolRepository(dataSource: SchoolDataSource): SchoolRepository {
        return SchoolRepository(dataSource)
    }

}