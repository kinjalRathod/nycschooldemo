package com.example.nycschooldemo.ui.schooldetailscreen

import android.content.Context
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.nycschooldemo.R
import com.example.nycschooldemo.data.model.School
import com.example.nycschooldemo.data.model.getFullAddress
import com.example.nycschooldemo.ui.UIState
import com.example.nycschooldemo.ui.generic.EmptyComponent
import com.example.nycschooldemo.ui.generic.ProgressIndicator
import com.example.nycschooldemo.ui.theme.*
import com.example.nycschooldemo.util.*
import com.example.nycschooldemo.viewmodels.SchoolSATDetailViewModel


@Composable
fun SchoolDetail(
    selectedSchool: School?, onBackPressed: (() -> Unit)
) {
    val viewModel: SchoolSATDetailViewModel = hiltViewModel()
    val satResults = viewModel.satResults.collectAsState().value
    val context = LocalContext.current

    Column(
        Modifier.padding(
            start = xlargeItemSpacing, top = xlargeItemSpacing, end = xlargeItemSpacing
        )
    ) {
        // Screen Title
        DetailHeader(schoolName = selectedSchool?.schoolName ?: "", onBackPressed)

        selectedSchool?.let {
            // Fetch SAT Results based on selected school
            viewModel.setData(selectedSchool)

            Column(
                Modifier
                    .fillMaxSize()
                    .wrapContentHeight()
                    .verticalScroll(rememberScrollState()),
                verticalArrangement = Arrangement.Top
            ) {
                Text(
                    text = selectedSchool.overviewParagraph ?: stringResource(R.string.na),
                    style = body,
                    modifier = Modifier
                        .fillMaxWidth()
                        .wrapContentHeight()
                )

                Spacer(Modifier.height(sItemSpacing))

                Divider(
                    modifier = Modifier
                        .height(0.5.dp)
                        .padding(vertical = mItemSpacing),
                    color = SemiDarkGray,
                )

                HorizontalDivider()

                ContactDetails(context, selectedSchool)

                HorizontalDivider()

                Text(text = stringResource(R.string.sat_results), style = title)

                Spacer(Modifier.height(largeItemSpacing))

                when (satResults) {
                    is UIState.Success -> {
                        val schoolDetail = satResults.data.getOrNull(0)
                        if (schoolDetail != null) {
                            Row(horizontalArrangement = Arrangement.SpaceEvenly) {
                                SATScore(
                                    score = schoolDetail.mathAvgScore
                                        ?: stringResource(R.string.na),
                                    label = stringResource(R.string.maths),
                                    Modifier.weight(1f)
                                )
                                SATScore(
                                    score = schoolDetail.writingAvgScore
                                        ?: stringResource(R.string.na),
                                    label = stringResource(R.string.writing),
                                    Modifier.weight(1f)
                                )
                                SATScore(
                                    score = schoolDetail.criticalReadingAvgScore
                                        ?: stringResource(R.string.na),
                                    label = stringResource(R.string.reading),
                                    Modifier.weight(1f)
                                )
                                Spacer(modifier = Modifier.size(20.dp))
                            }
                        } else {
                            SATDetailErrorComponent()
                        }
                    }
                    is UIState.Error -> {
                        Column(
                            Modifier.height(40.dp), verticalArrangement = Arrangement.Center
                        ) {
                            EmptyComponent(
                                description = R.string.sat_data_not_found,
                                iconRes = R.drawable.ic_something_went_wrong,
                                iconSize = 40.dp
                            )
                        }
                    }
                    is UIState.Loading -> {
                        Box(Modifier.wrapContentHeight()) {
                            ProgressIndicator()
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun SATDetailErrorComponent() {
    Column(
        Modifier.defaultMinSize(40.dp), verticalArrangement = Arrangement.Center
    ) {
        EmptyComponent(
            description = R.string.sat_data_not_found
        )
    }
}

@Composable
fun ContactDetails(context: Context, selectedSchool: School) {
    Text(text = stringResource(R.string.contact_details), style = title)
    Spacer(Modifier.height(largeItemSpacing))

    selectedSchool.apply {
        val zero = stringResource(R.string.zero)
        ContactItem(
            label = stringResource(R.string.address), data = getFullAddress()
        ) {
            context.openGoogleMaps(
                (latitude ?: zero).toFloat(),
                (longitude ?: zero).toFloat()
            )
        }

        ContactItem(
            label = stringResource(R.string.phone), data = "$phoneNumber"
        ) {
            context.openDial("$phoneNumber")
        }

        ContactItem(
            label = stringResource(R.string.email), data = "$schoolEmail"
        ) {
            context.openEmail("$schoolEmail")
        }

        ContactItem(
            label = stringResource(R.string.fax), data = "$faxNumber", false
        )
    }
}

@Composable
fun DetailHeader(schoolName: String, onBackPressed: () -> Unit) {
    Row(
        Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically
    ) {
        Icon(painter = painterResource(id = R.drawable.ic_arrow_back),
            contentDescription = null,
            modifier = Modifier
                .padding(end = 8.dp)
                .size(20.dp)
                .clickable {
                    onBackPressed.invoke()
                })
        Text(
            text = schoolName,
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight(),
            style = heading,
            color = Blue,
            textAlign = TextAlign.Start
        )
    }
}

@Composable
fun SATScore(score: String, label: String, modifier: Modifier) {
    Column(modifier) {
        Text(
            text = score, style = scoreText, color = PrimaryFontColor
        )
        Text(
            text = label, style = scoreLabel, color = SecondaryFontColor
        )
    }
}

@Composable
fun HorizontalDivider() {
    Divider(
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = largeItemSpacing),
        color = LightGray,
        thickness = 1.dp
    )
}

@Composable
fun ContactItem(
    label: String, data: String, isHyperLink: Boolean = true, onClick: (() -> Unit)? = null
) {
    Row {
        Text(text = label, style = body, modifier = Modifier.weight(0.2f))
        Text(text = data,
            style = body,
            color = if (isHyperLink) HyperLink else PrimaryFontColor1,
            modifier = Modifier
                .weight(0.8f)
                .clickable { onClick?.invoke() })
    }
    Spacer(Modifier.height(sItemSpacing))
}
