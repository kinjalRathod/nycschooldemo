package com.example.nycschooldemo.ui.schoollistscreen

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.nycschooldemo.R
import com.example.nycschooldemo.data.model.School
import com.example.nycschooldemo.data.model.getShortAddress
import com.example.nycschooldemo.ui.UIState
import com.example.nycschooldemo.ui.generic.EmptyComponent
import com.example.nycschooldemo.ui.generic.ProgressIndicator
import com.example.nycschooldemo.ui.theme.*
import com.example.nycschooldemo.viewmodels.SchoolViewModel

@Composable
fun SchoolList(navigateToDetail: (school: School) -> Unit) {
    val viewModel: SchoolViewModel = hiltViewModel()
    val schoolState = viewModel.list.collectAsState().value

    Column(
        Modifier
            .fillMaxSize()
            .background(bg)
    ) {
        ListHeader()
        when (schoolState) {
            is UIState.Loading -> {
                Box(Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
                    ProgressIndicator()
                }
            }
            is UIState.Success -> {
                if (schoolState.data.isEmpty()) {
                    EmptyComponent(
                        header = R.string.no_data_found,
                        iconRes = R.drawable.ic_something_went_wrong
                    )
                } else {
                    LazyColumn(
                        modifier = Modifier.padding(16.dp),
                        verticalArrangement = Arrangement.spacedBy(16.dp),
                    ) {
                        items(schoolState.data) {
                            SchoolItem(it, navigateToDetail)
                        }
                    }
                }
            }
            is UIState.Error -> {
                EmptyComponent(
                    iconRes = R.drawable.ic_something_went_wrong,
                    header = R.string.error_msg, btnLabel = R.string.refresh
                ) {
                    viewModel.refresh()
                }
            }
        }
    }
}

@Composable
fun SchoolItem(
    item: School,
    navigateToDetail: (school: School) -> Unit
) {
    item.apply {
        Card(Modifier) {
            Column(
                Modifier
                    .clip(RoundedCornerShape(8.dp))
                    .background(White)
                    .clickable {
                        navigateToDetail.invoke(this)
                    }
                    .padding(12.dp)) {
                Text("$schoolName", style = itemTitle)
                Row(
                    Modifier
                        .fillMaxWidth(),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Icon(
                        modifier = Modifier
                            .padding(top = 5.dp)
                            .size(15.dp),
                        painter = painterResource(id = R.drawable.ic_location),
                        contentDescription = null,
                        tint = SemiDarkGray
                    )
                    Text(
                        modifier = Modifier.padding(start = 8.dp),
                        text = getShortAddress(),
                        style = body
                    )
                }
            }
        }
    }
}

@Composable
fun ListHeader() {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .height(60.dp)
            .background(Blue),
        contentAlignment = Alignment.Center
    ) {
        Text(
            text = stringResource(R.string.nyc_school),
            style = extraLargeMedium,
            color = White,
            textAlign = TextAlign.Center
        )
    }
}