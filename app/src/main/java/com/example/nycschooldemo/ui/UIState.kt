package com.example.nycschooldemo.ui

/**
 * Class to reflect UI state
 * */
sealed class UIState<out T> {
    /** Class for Loading State */
    object Loading : UIState<Nothing>()

    /**
     * Class for Success State
     * */
    data class Success<out T>(val data: T) : UIState<T>()

    /** Class for Error State
     * @param e
     * */
    data class Error(val e: Throwable?) : UIState<Nothing>()
}
