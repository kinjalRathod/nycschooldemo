package com.example.nycschooldemo.util

import androidx.compose.ui.unit.dp

const val API_PATH = "resource"
const val KEY_SCHOOL = "school"

val xsItemSpacing = 8.dp
val sItemSpacing = 8.dp
val mItemSpacing = 10.dp
val largeItemSpacing = 12.dp
val xlargeItemSpacing = 14.dp
val xxlargeItemSpacing = 18.dp