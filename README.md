NYC School

ScreenShots
----------------------------

<img src="screenshots/app_demo.gif" width="120"/>
<img src="screenshots/screen_school_list.jpg" width="120"/>
<img src="screenshots/screen_school_detail.jpg" width="120"/>
<img src="screenshots/screen_school_detail_sat_data.jpg" width="120"/>
<img src="screenshots/screen_school_detail_sat_unavailable.jpg" width="120"/>
<img src="screenshots/screen_empty.jpg" width="120"/>


About
----------------------------

This app shows a list of schools in the NYC district along with the most recent SAT school level scores for New York City.
Results at the school level for 2012 graduating seniors are available.

Features
----------------------------

- User can view schools of NYC district
- User can find details of each school
- User can view school-level SAT results of 2012


Tech Used
---------------------------

- Kotlin
- Jetpack Compose
- MVVM Architecture
- Hilt as Dependency Injection
- Unit Test using Mockito
- Retrofit
- Moshi




